<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xanyfield.js');

?>
<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_form,
    'form_type'    => 'ajax',
]);?>

<input type="hidden" name="id_fieldset" value="<?=$id_fieldset?>">
<input type="hidden" name="value_assign_key" value="<?=$value_assign_key?>">
<input type="hidden" name="form_hash" value="<?=$form_hash?>">


<?php foreach ($field_list as $id_field => $item):?>
    <!-- id_field <?=$id_field;?> -->
    <?=$item;?>
    <!-- /id_field <?=$id_field;?> -->
<?php endforeach;?>

<button type="submit"
        class="btn btn-block btn-primary mb-4">Отправить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>
