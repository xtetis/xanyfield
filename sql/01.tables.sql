
DROP TABLE IF EXISTS `xanyfield_fieldset`;
CREATE TABLE IF NOT EXISTS `xanyfield_fieldset` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Первичный ключ',
  `name` varchar(100) NOT NULL COMMENT 'Имя компонента',
  `form_js_success` text DEFAULT NULL COMMENT 'JS, который выполнить после успешной валидации формы',
  `form_js_fail` text DEFAULT NULL COMMENT 'JS, который выполнить после нейспешной валидации формы',
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Дата/время создания записи',
  PRIMARY KEY (`id`),
  UNIQUE KEY `xanyfield_fieldset_UN` (`name`),
  KEY `afield_component_name_IDX` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='Список компонентов для дополнительных полей';
