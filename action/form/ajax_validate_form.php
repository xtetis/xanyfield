<?php

/**
 * Этот скрипт обрабатывает запрос, связанный с валидацией формы "xanyfield", 
 * и возвращает результат в формате JSON.
 * 
 * /engine/vendor/xtetis/xanyfield/action/form/ajax_validate_form.php
 */

// Если константа 'SYSTEM' не определена, это означает, что скрипт был вызван напрямую (не через основной индексный файл). 
// В таком случае выполнение скрипта прекращается для защиты от несанкционированного доступа.
if (!defined('SYSTEM')) 
{
    die('Не разрешен просмотр'); // Сообщение о том, что доступ запрещен.
}

// Получение POST-параметров из запроса с помощью помощника RequestHelper.
// Функция поста принимает три параметра: имя параметра, тип данных и значение по умолчанию.
$id_fieldset      = \xtetis\xengine\helpers\RequestHelper::post('id_fieldset', 'int', 0); // ID набора полей (формы), если не передан — по умолчанию 0.
$value_assign_key = \xtetis\xengine\helpers\RequestHelper::post('value_assign_key', 'int', 0); // Ключ привязки значений, если не передан — по умолчанию 0.
$form_hash        = \xtetis\xengine\helpers\RequestHelper::post('form_hash', 'str', ''); // Хеш формы для защиты от подделки запросов, по умолчанию пустая строка.

// Создание экземпляра модели FieldsetModel для работы с формой.
// Передаем полученные данные (id, ключ значения и хеш) в конструктор модели.
$model_fieldset = new \xtetis\xanyfield\models\FieldsetModel([
    'id'               => $id_fieldset,         // Идентификатор набора полей.
    'value_assign_key' => $value_assign_key,    // Ключ для привязки значений.
    'form_hash'        => $form_hash,           // Хеш формы для проверки безопасности.
]);

// Проверка формы на корректность с помощью метода validateForm() модели.
if ($model_fieldset->validateForm()) 
{
    // Если форма прошла валидацию, создаем положительный ответ.
    $response['js_success'] = $model_fieldset->form_js_success; // Сценарий, который нужно выполнить при успешной валидации.
    $response['result'] = true; // Установка флага успешной валидации.
}
else
{
    // Если валидация не прошла, формируем список ошибок.
    $response['errors'] = $model_fieldset->getErrors(); // Получение списка ошибок, если форма не валидна.
    $response['form_js_fail'] = $model_fieldset->form_js_fail; // Сценарий, который нужно выполнить при ошибке валидации.
}

// Преобразование массива с ответом в формат JSON с использованием JsonHelper и вывод его пользователю.
echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);

