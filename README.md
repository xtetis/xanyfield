# xtetis\xanyfield
## Модуль для xEngine для сохранения данных форм (формы хранятся в БД)


Powered by xTetis

---
## Установка модуля

 - Если Вы используете xEngine (https://bitbucket.org/xtetis/xengine/), то он содержит в ноде require файла composer.json, потому будет установлен автоматически
```sh
"xtetis/xanyfield": "dev-master",
```

Пример использования (рендер формы)

```php
<?=\xtetis\xanyfield\Component::renderForm(
    [
        'component'=>'user',
        'component_category'=>'info',
        'value_assign_key'=>1
    ]
);
```

## Обратная связь

Для связи с автором автором
skype: xtetis
telegram: @xtetis
