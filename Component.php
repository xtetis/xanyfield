<?php

namespace xtetis\xanyfield;

class Component extends \xtetis\xengine\models\Component
{


    /**
     * @param array $params
     */
    public function __construct($params = [])
    {
        // Проверяет параметры
        \xtetis\xanyfield\Config::validateParams();
        
        parent::__construct($params);
    }
    
    /**
     * Рендер формы
     * 
     * Обязательные параметры
     *   [
     *      'component'=>'user',
     *      'component_category'=>'info',
     *      'value_assign_key'=>1
     *   ]
     */
    public static function renderForm($params = [])
    {
        $model_form = new \xtetis\xanyfield\models\FormModel($params);
        $form = $model_form->renderForm();
        if (!$form)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Ошибка при создании формы xtetis\xanyfield: '.$model_form->getLastErrorMessage());
        }

        return $form;
    }

}
