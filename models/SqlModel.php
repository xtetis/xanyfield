<?php

namespace xtetis\xanyfield\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Проверка существования компонента xanyfield
     * Возвращает ID компонента
     */
    public static function xanyfieldIsComponentExists(
        $component_name = '',
        $component_category = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_is_component_exists(:component_name,:component_category,@result,@result_str)');

        $stmt->bindParam('component_name', $component_name, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('component_category', $component_category, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Проверка существования компонента xanyfield
     */
    /*
    public static function xanyfieldGetFieldListByComponent(
        $id_component = 0
    )
    {

        $id_component = intval($id_component);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_get_fields_by_component(:id_component)');

        $stmt->bindParam('id_component', $id_component, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $rows = $stmt->fetchAll();

        if (!is_array($rows))
        {
            $rows = [];
        }

        return $rows;
    }
    */

    /**
     * Проверка существования поля xanyfield
     * Возвращает параметры поля
     */
    public static function xanyfieldGetFieldInfo(
        $id_field = 0
    )
    {

        $id_field = intval($id_field);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_get_field_info(:id_field,@result,@result_str,@id_field_category,@field_category_name,@title,@name,@params)');

        $stmt->bindParam('id_field', $id_field, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str, @id_field_category as id_field_category, @field_category_name as field_category_name, @title as title, @name as name, @params as params; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']              = $row['result'];
        $ret['result_str']          = $row['result_str'];
        $ret['id_field_category']   = $row['id_field_category'];
        $ret['field_category_name'] = $row['field_category_name'];
        $ret['title']               = $row['title'];
        $ret['name']                = $row['name'];
        $ret['params']              = $row['params'];

        return $ret;
    }

    /**
     * Получение сохраненного значения поля afield
     */
    public static function getStoredFieldValue(
        $id_field = 0,
        $id_fieldset = 0,
        $value_assign_key = 0
    )
    {

        $id_field         = intval($id_field);
        $id_fieldset      = intval($id_fieldset);
        $value_assign_key = intval($value_assign_key);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_get_field_value(:id_field,:id_fieldset,:value_assign_key,@result,@result_str)');

        $stmt->bindParam('id_field', $id_field, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('id_fieldset', $id_fieldset, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('value_assign_key', $value_assign_key, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Проверка существования компонента xanyfield
     * Возвращает параметры поля
     */
    /*
    public static function xanyfieldGetComponentInfo(
        $id_component = 0
    )
    {

        $id_field = intval($id_component);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_get_component_info(:id_component,@result,@result_str,@category,@form_js_success,@form_js_fail)');

        $stmt->bindParam('id_component', $id_component, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str, @category as category, @form_js_success as form_js_success, @form_js_fail as form_js_fail; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']          = $row['result'];
        $ret['result_str']      = $row['result_str'];
        $ret['category']        = $row['category'];
        $ret['form_js_success'] = $row['form_js_success'];
        $ret['form_js_fail']    = $row['form_js_fail'];

        return $ret;
    }
    */

    /**
     * Проверка существования компонента xanyfield
     * Возвращает параметры поля
     */
    public static function xanyfieldSaveValue(
        $id_fieldset = 0,
        $id_field = 0,
        $value_assign_key = 0,
        $value = ''
    )
    {

        $id_fieldset      = intval($id_fieldset);
        $id_field         = intval($id_field);
        $value_assign_key = intval($value_assign_key);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xanyfield_save_value(:id_fieldset,:id_field,:value_assign_key,:value,@result,@result_str)');

        $stmt->bindParam('id_fieldset', $id_fieldset, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('id_field', $id_field, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('value_assign_key', $value_assign_key, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('value', $value, \PDO::PARAM_LOB);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Возвращает данные из xanyfield_component по ID
     */
    public static function getXanyfieldComponentById(
        $id = 0,
    )
    {

        $id = intval($id);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('SELECT * FROM xanyfield_component WHERE id = :id;');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $row = $stmt->fetch();

        return $row;
    }

}
