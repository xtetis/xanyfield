<?php

namespace xtetis\xanyfield\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class FieldModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID
     */
    public $id = 0;

    /**
     * ID набора полей (используется для получения значения)
     */
    public $id_fieldset = 0;

    /**
     * Связанный ID (используется для получения значения)
     */
    public $value_assign_key = 0;

    /**
     * Заголовок
     */
    public $title = '';

    /**
     * Имя поля
     */
    public $name = '';

    /**
     * @var string
     */
    public $table_name = 'xanyfield_field';

    /**
     * Значение поля
     */
    public $value = '';

    /**
     * Параметры поля (JSON)
     */
    public $params = '';

    /**
     * Параметры поля (массив)
     */
    public $params_array = [];

    /**
     * Отрендеренный HTML c полем
     */
    public $rendered_field = '';

    public $field_params = [];

    public $field_actions_list = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        // Проверяет параметры
        \xtetis\xanyfield\Config::validateParams();

        parent::__construct($params);
    }

    /**
     * Получает из БД сохраненное ранее значение
     */
    public function getStoredValue()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id               = intval($this->id);
        $this->value_assign_key = intval($this->value_assign_key);
        $this->id_fieldset      = intval($this->id_fieldset);

        if (!$this->id)
        {
            $this->addError('id', 'Для FieldModel не указан ID');

            return false;
        }

        if (!$this->value_assign_key)
        {
            $this->addError('value_assign_key', 'Для FieldModel не указан value_assign_key');

            return false;
        }

        if (!$this->id_fieldset)
        {
            $this->addError('id_fieldset', 'Для FieldModel не указан id_fieldset');

            return false;
        }

        // Получение значения поля
        $row = \xtetis\xanyfield\models\SqlModel::getStoredFieldValue(
            $this->id,
            $this->id_fieldset,
            $this->value_assign_key
        );

        // Если ошибка при получении значения поля
        if ($row['result'] < 0)
        {

            $this->addError('params', $row['result_str']);

            return false;
        }

        $this->value = strval($row['result_str']);

        // Если тип поля поле это галерея (input_image), даже если значение
        // не установлено -создаем галерею и присваиваем полю значение ID галереи
        $this->setGalleryValue();

        return true;
    }

    /**
     * Если тип поля поле это галерея (input_image), даже если значение
     * не установлено -создаем галерею и присваиваем полю значение ID галереи
     */
    public function setGalleryValue()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->value_assign_key = intval($this->value_assign_key);
        $this->id_fieldset      = intval($this->id_fieldset);

        if (!strlen($this->params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' не указан params');

            return false;
        }

        if (!\xtetis\xengine\helpers\JsonHelper::isJson($this->params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' params не является валидным JSON');

            return false;
        }
        $this->field_params = json_decode($this->params, true);

        if (!is_array($this->field_params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' params не является массивом');

            return false;
        }

        if (!isset($this->field_params['template']))
        {
            $this->addError('params', 'В параметрах поля #' . $this->id . ' не указана нода template');

            return false;
        }

        if (!is_string($this->field_params['template']))
        {
            $this->addError('params', 'В параметрах поля #' . $this->id . ' нода template не является строкой');

            return false;
        }

        if (!strlen($this->field_params['template']))
        {
            $this->addError('params', 'В параметрах поля #' . $this->id . ' не заполнена нода template');

            return false;
        }

        // Если шаблон не ввод изображения (галерея)
        if ('input_image' != $this->field_params['template'])
        {
            return true;
        }

        if (!$this->value_assign_key)
        {
            $this->addError('params', 'Для поля #' . $this->id . ' не указан value_assign_key');

            return false;
        }

        // Если это галерея - то значение INTEGER (ID галереи)
        $this->value = intval($this->value);

        // Если ID уже есть, считаем галерею созданной и проверяем наличие такой галереи
        if ($this->value)
        {
            // Если галарея с таком ID не найдена
            if (!\xtetis\ximg\models\GalleryModel::generateModelById($this->value))
            {
                $this->addError('name', 'В поле #' . $this->id . ' значение ' . $this->value . ', такой галереи не существует (нарушена цклостность)');

                return false;
            }
        }
        else
        {
            // Если такой галереи нет - создаем её согласно правилам
            $gallery_model = new \xtetis\ximg\models\GalleryModel(
                [

                    'id_category' => XANYFIELD_PARENT_GALLERY_CATEGORY,
                    'name'        => 'xanyfield_field_' . $this->id . '_assign_key_' . $this->value_assign_key,
                ]
            );

            if (!$gallery_model->addGallery())
            {
                $this->addError('gallery_id', $gallery_model->getLastErrorMessage());

                return false;
            }

            $this->value = $gallery_model->id;

            return true;
        }
    }

    /**
     * @return mixed
     */
    public function renderField()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Валидирует параметры поля, полученные из БД
        if (!$this->validateFieldParams())
        {
            return false;
        }

        $params = [
            'template'   => $this->field_params['template'],
            'value'      => $this->value,
            'attributes' => $this->field_params,
        ];

        $this->rendered_field = \xtetis\xform\Component::renderField(
            $params
        );

        return $this->rendered_field;
    }

    /**
     * Валидирует параметры поля, полученные из БД
     */
    public function validateFieldParams()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id     = strval($this->id);
        $this->name   = strval($this->name);
        $this->params = strval($this->params);
        $this->title  = strval($this->title);

        if (!$this->id)
        {
            $this->addError('id', 'Для поля не указан id');

            return false;
        }

        if (!strlen($this->name))
        {
            $this->addError('name', 'Для поля #' . $this->id . ' не указан name');

            return false;
        }

        if (!strlen($this->title))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' не указан title');

            return false;
        }

        if (!strlen($this->params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' не указан params');

            return false;
        }

        if (!\xtetis\xengine\helpers\JsonHelper::isJson($this->params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' params не является валидным JSON');

            return false;
        }
        $this->field_params = json_decode($this->params, true);

        if (!is_array($this->field_params))
        {
            $this->addError('params', 'Для поля #' . $this->id . ' params не является массивом');

            return false;
        }

        if (!isset($this->field_params['template']))
        {
            $this->addError('params', 'В параметрах поля #' . $this->id . ' не указана нода template');

            return false;

        }

        if (!is_string($this->field_params['template']))
        {
            $this->addError('params', 'В параметрах поля #' . $this->id . ' нода template не является строкой');

            return false;
        }

        if (!strlen($this->field_params['template']))
        {
            $this->addError('id', 'В параметрах поля #' . $this->id . ' не заполнена нода template');

            return false;
        }

        $this->field_params['label'] = $this->title;
        $this->field_params['name']  = 'xanyfield_' . $this->id . '';

        return true;
    }

    /**
     * Валидирует значение поля
     *
     * Обязательные аттрибуты при создании объекта
     *      id
     *      id_component
     *      value_assign_key
     *      value
     */
    public function validateValue()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Валидирует параметры поля, полученные из БД
        if (!$this->validateFieldParams())
        {
            return false;
        }

        // Валидируем согласно правил
        if (!$this->validateByRules())
        {
            return false;
        }

        return true;
    }

    /**
     * Валидируем полученное значение согласно правил
     */
    public function validateByRules()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Если тип поля - SELECT - тогда получаем список
        if ('select' == $this->field_params['template'])
        {
            $this->value = intval($this->value);
            if (!isset($this->field_params['options'][$this->value]))
            {
                $this->addError('options', 'Недопустимое значение');

                return false;
            }
        }
        else
        {
            if (isset($this->field_params['validate']['type']))
            {
                if ('string' == $this->field_params['validate']['type'])
                {
                    $this->value = strval($this->value);

                    if (isset($this->field_params['validate']['actions']))
                    {
                        $this->field_actions_list = explode(':', strval($this->field_params['validate']['actions']));

                        foreach ($this->field_actions_list as $action)
                        {
                            if ('trim' == $action)
                            {
                                $this->value = trim($this->value);
                            }

                            if ('strip_tags' == $action)
                            {
                                $this->value = strip_tags($this->value);
                            }
                        }
                    }

                    if (isset($this->field_params['validate']['min_length']))
                    {
                        $this->field_params['validate']['min_length'] = intval($this->field_params['validate']['min_length']);

                        if (mb_strlen($this->value) < $this->field_params['validate']['min_length'])
                        {
                            $this->addError('min_length', 'Минимальная длина ' . $this->field_params['validate']['min_length'] . ' символов ');

                            return false;
                        }
                    }

                    if (isset($this->field_params['validate']['max_length']))
                    {
                        $this->field_params['validate']['max_length'] = intval($this->field_params['validate']['max_length']);

                        if (mb_strlen($this->value) > $this->field_params['validate']['max_length'])
                        {
                            $this->addError('min_length', 'Максимальная длина ' . $this->field_params['validate']['max_length'] . ' символов');

                            return false;
                        }
                    }

                }
                elseif ('integer' == $this->field_params['validate']['type'])
                {
                    $this->value = intval($this->value);

                    if (isset($this->field_params['validate']['actions']))
                    {
                        $this->field_actions_list = explode(':', strval($this->field_params['validate']['actions']));

                        foreach ($this->field_actions_list as $action)
                        {
                            if ('intval' == $action)
                            {
                                $this->value = intval($this->value);
                            }
                        }
                    }

                    if (isset($this->field_params['validate']['min']))
                    {
                        $this->field_params['validate']['min'] = intval($this->field_params['validate']['min']);

                        if ($this->value < $this->field_params['validate']['min'])
                        {
                            $this->addError('min', 'Минимальное значение ' . $this->field_params['validate']['min'] . ' ');

                            return false;
                        }
                    }

                    if (isset($this->field_params['validate']['max']))
                    {
                        $this->field_params['validate']['max'] = intval($this->field_params['validate']['max']);

                        if ($this->value > $this->field_params['validate']['max'])
                        {
                            $this->addError('max', 'Максимальное значение ' . $this->field_params['validate']['max'] . '');

                            return false;
                        }
                    }
                }
                elseif ('date' == $this->field_params['validate']['type'])
                {
                    $this->value = strval($this->value);
                    if (preg_replace('/[^0-9\.]/', '', $this->value) != $this->value)
                    {
                        $this->addError('value', 'Тип date подразумевает наличие цифры и точки');

                        return false;
                    }

                    if (mb_strlen($this->value) != 10)
                    {
                        $this->addError('min_length', 'Длина поля типа date должна равняться 10 символам');

                        return false;
                    }

                    if (!preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $this->value))
                    {
                        $this->addError('value', 'Некорректный формат даты');

                        return false;
                    }

                    list($d, $m, $y) = array_pad(explode('.', $this->value, 3), 3, 0);

                    if (!ctype_digit("$d$m$y"))
                    {
                        $this->addError('value', 'Некорректная дата');

                        return false;
                    }

                    if (!checkdate($m, $d, $y))
                    {
                        $this->addError('value', 'Некорректная дата');

                        return false;
                    }
                }
            }
        }

        return true;
    }

}
