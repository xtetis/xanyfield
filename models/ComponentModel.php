<?php

namespace xtetis\xanyfield\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/*

Для строки

[
    'type'=>'string' - преобразовать в строку
    'min_length'=>1,
    'max_length'=>100,
    'actions'=>'trim:strip_tags',
]

 */
class ComponentModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID поля
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'xanyfield_component';

    /**
     * @var string
     */
    public $name = '';
    /**
     * @var string
     */
    public $category = '';
    /**
     * @var string
     */
    public $form_js_success = '';
    /**
     * @var string
     */
    public $form_js_fail = '';
    /**
     * @var string
     */
    public $create_date = '';

    /**
     * Результат получения списка полей по компоненту
     */
    public $xanyfield_get_field_list_by_component_result = [];

    /**
     * Результат получения данных по ID
     */
    public $get_xanyfield_component_by_id_result = [];

    /**
     * Список моделей полей xanyfield для данного компонента
     */
    public $model_xanyfield_field_list = [];

    /**
     * Конструктор
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'id',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

    }

    /**
     * Ищем по ID
     */
    public function getById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$this->id)
        {
            $this->addError('common', 'Не указан ID');

            return false;
        }

        $this->get_xanyfield_component_by_id_result = \xtetis\xanyfield\models\SqlModel::getXanyfieldComponentById($this->id);

        if (!$this->get_xanyfield_component_by_id_result)
        {
            $this->addError('common', 'Указанный компонент не найден');

            return false;
        }

        $this->name            = $this->get_xanyfield_component_by_id_result['name'];
        $this->category        = $this->get_xanyfield_component_by_id_result['category'];
        $this->form_js_success = $this->get_xanyfield_component_by_id_result['form_js_success'];
        $this->form_js_fail    = $this->get_xanyfield_component_by_id_result['form_js_fail'];
        $this->create_date     = $this->get_xanyfield_component_by_id_result['create_date'];

        return true;
    }

    /**
     * Заполняет массив ID полей для указанного компонента
     */
    public function getIdFieldList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Получаем список полей для указанного компонента
        $this->xanyfield_get_field_list_by_component_result = \xtetis\xanyfield\models\SqlModel::xanyfieldGetFieldListByComponent(
            $this->id
        );

        if ($this->xanyfield_get_field_list_by_component_result)
        {
            foreach ($this->xanyfield_get_field_list_by_component_result as $key => $value)
            {
                $model_xanyfield_field = new \xtetis\xanyfield\models\FieldModel([
                    'id'           => $value['id_field'],
                    'id_component' => $this->id,
                ]);

                $model_xanyfield_field->setFieldInfo();

                if ($model_xanyfield_field->getErrors())
                {
                    $this->addError('model_xanyfield_field', __FUNCTION__ . ':' . $model_xanyfield_field->getLastErrorMessage());

                    return false;
                }

                $this->model_xanyfield_field_list[$value['id_field']] = $model_xanyfield_field;
            }
        }

        return true;
    }
}
