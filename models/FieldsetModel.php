<?php

namespace xtetis\xanyfield\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class FieldsetModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Связанный ID
     */
    public $value_assign_key = 0;

    /**
     * Имя набора полей
     */
    public $name = '';

    /**
     * @var string
     */
    public $table_name = 'xanyfield_fieldset';

    /**
     * Массив связанных моделей полей
     */
    protected $assigned_field_model_list = [];

    /**
     * Хэш формы (проверяем при валидации формы)
     */
    public $form_hash = '';

    /**
     * JS который выполнить при успешной валидации формы
     */
    public $form_js_success = '';

    /**
     * JS который выполнить в случае ошибок
     */
    public $form_js_fail = '';

    /**
     * Возвращает HTML отрендеренной формы
     */
    public function renderForm($value_assign_key = 0)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id               = intval($this->id);
        $this->value_assign_key = intval($value_assign_key);

        if (!$this->id)
        {
            $this->addError('id', 'Для FieldsetModel не указан ID');

            return false;
        }

        // Получает список моделей полей для указанного Fieldset
        

        $rendered_fields_html = [];
        // Рендерим поля
        foreach ($this->getFieldModelList() as $id_field => $model_field)
        {
            // Устанавливаем параметры, по которым искать значение
            $model_field->id_fieldset      = $this->id;
            $model_field->value_assign_key = $this->value_assign_key;

            // Получаем значение поля
            if (!$model_field->getStoredValue())
            {
                $this->addError('common', $model_field->getLastErrorMessage());

                return false;
            }

            // Рендерим поле
            $rendered_fields_html[$model_field->id] = $model_field->renderField();
        }

        // Урл страницы для валидации формы
        $url_validate_form = \xtetis\xanyfield\Component::makeUrl([
            'path' => [
                'form',
                'ajax_validate_form',
            ],
        ]);

        return \xtetis\xanyfield\Component::renderBlock(
            'form/form',
            [
                'field_list'        => $rendered_fields_html,
                'id_fieldset'       => $this->id,
                'value_assign_key'  => $this->value_assign_key,
                'form_hash'         => $this->getFormHash(),
                'url_validate_form' => $url_validate_form,
            ]
        );
    }

    /**
     * Возвращает хеш формы
     */
    public function getFormHash()
    {
        return md5($this->name . $this->value_assign_key);
    }

    /**
     * Получает список моделей полей для указанного Fieldset
     */
    public function getFieldModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if ($this->assigned_field_model_list)
        {
            return $this->assigned_field_model_list;
        }

        $sql = '
            SELECT id_field id
            FROM xanyfield_field_in_fieldset t
            WHERE t.id_fieldset  = :id_fieldset
        ';

        $params = [
            'id_fieldset' => $this->id,
        ];

        $this->assigned_field_model_list = \xtetis\xanyfield\models\FieldModel::getModelListBySql(
            $sql, 
            $params,
            [
                'cache'=>true,
                'cache_tags'=>[
                    'xanyfield_field_in_fieldset',
                ]
            ]
        );

        return $this->assigned_field_model_list;
    }

    /**
     * Валидация формы
     */
    public function validateForm()
    {
        $this->id               = intval($this->id);
        $this->value_assign_key = intval($this->value_assign_key);
        $this->form_hash        = strval($this->form_hash);

        if (!$this->id)
        {
            $this->addError('id', 'При валидации формы отсутствует id_fieldset');

            return false;
        }

        if (!$this->value_assign_key)
        {
            $this->addError('value_assign_key', 'При валидации формы отсутствует value_assign_key');

            return false;
        }

        if (!strlen($this->form_hash))
        {
            $this->addError('form_hash', 'При валидации формы отсутствует form_hash');

            return false;
        }

        // Возвращает модель по ID или false, в случае неудачи
        $model_fieldset = \xtetis\xanyfield\models\FieldsetModel::generateModelById($this->id);

        if (!$model_fieldset)
        {
            $this->addError('id', 'При валидации формы указан несуществующий id_fieldset');

            return false;
        }

        $model_fieldset->value_assign_key = $this->value_assign_key;
        $model_fieldset->form_hash        = $this->form_hash;

        if ($model_fieldset->getFormHash() != $model_fieldset->form_hash)
        {
            $this->addError('id', 'При валидации формы указан некорректный id_fieldset');

            return false;
        }


        // Валидируем значения полей
        if (!$model_fieldset->validateFields())
        {
            $this->errors = $model_fieldset->errors;

            return false;
        }

        // Сохраняем значения полей
        if (!$model_fieldset->saveFields())
        {
            $this->errors = $model_fieldset->errors;
            
            return false;
        }

        return true;

    }

    /**
     * Валидирует все поля, который пришли как POST параметры
     */
    public function validateFields()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Проходим по всем полям, которые привязаны к текущему Fieldset
        foreach ($this->getFieldModelList() as $id_field => &$model_field)
        {
            // Проставляем текущие параметры
            $field_value                   = \xtetis\xengine\helpers\RequestHelper::post(
                'xanyfield_' . $id_field, 
                'raw', 
                ''
            );
            $model_field->value            = $field_value;
            $model_field->value_assign_key = $this->value_assign_key;
            $model_field->id_fieldset      = $this->id;

            // Вализируем значение
            if (!$model_field->validateValue())
            {
                $this->addError('xanyfield_' . $id_field, $model_field->getLastErrorMessage());
            }
        }

/*
        // Получаем список полей для указанного компонента
        $this->xanyfield_get_field_list_by_component_result = \xtetis\xanyfield\models\SqlModel::xanyfieldGetFieldListByComponent(
            $this->id_component
        );

        foreach ($this->xanyfield_get_field_list_by_component_result as &$item)
        {
            $item['id_field'] = intval($item['id_field']);

            $field_value = \xtetis\xengine\helpers\RequestHelper::post('xanyfield_' . $item['id_field'], 'raw', '');

            $params = [
                'id'               => $item['id_field'],
                'id_component'     => $this->id_component,
                'value_assign_key' => $this->value_assign_key,
                'value'            => $field_value,
            ];
            $this->model_field = new \xtetis\xanyfield\models\FieldModel($params);

            if (!$this->model_field->validateValue())
            {
                $this->addError('xanyfield_' . $item['id_field'], $this->model_field->getLastErrorMessage());

            }

            $this->validated_values[$item['id_field']] = $this->model_field->value;
        }
        */

        return true;
    }

    /**
     * Сохраняет значение полей
     */
    public function saveFields()
    {

        if ($this->getErrors())
        {
            return false;
        }


        foreach ($this->getFieldModelList() as $id_field => &$model_field)
        {




            // Сохраняем значение поля
            $row = \xtetis\xanyfield\models\SqlModel::xanyfieldSaveValue(
                $this->id,
                $model_field->id,
                $model_field->value_assign_key,
                $model_field->value
            );


            // Если компонент xanyfield не существует
            if ($row['result'] < 0)
            {
                $this->addError('xanyfield_' . $model_field->id, $row['result_str']);

                return false;
            }
        }

        return true;
    }



    /**
     * Возвращает привязанную модель Fieldset по имени или false, если не найдена
     */
    public function getFieldModelByName($field_name = '')
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Перебираем привязанные модели Field
        $current_field_model = false;
        foreach ($this->getFieldModelList() as $id_field => $model_field) 
        {
            if ($field_name == $model_field->name)
            {
                $current_field_model = $model_field;
                break;
            }
        }

        // Если модель найдена
        if ($current_field_model)
        {
            return $current_field_model;
        }

        return false;

    }

}
