<?php

namespace xtetis\xanyfield\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class FormModel extends \xtetis\xengine\models\Model
{

    /**
     * Компонент
     */
    public $component = '';

    /**
     * Категория компонента
     */
    public $component_category = '';

    /**
     * Ключ привязки значения (по которому сохраняется значение поля)
     */
    public $value_assign_key = 0;

    /**
     * ID компонента
     */
    public $id_component = 0;

    /**
     * Результат существования компонента xanyfield
     */
    public $xanyfield_is_component_exists_result = [];

    /**
     * Результат получения списка полей по компоненту
     */
    public $xanyfield_get_field_list_by_component_result = [];

    /**
     * Список полей для формы
     * [
     *      // ключ = ID категории (если 0, то без категории)
     *      1=>[
     *          'field_category_name'=> 'ZZZ',
     *          'fields' => [
     *              // ключ = ID поля
     *              1 => [
     *                      'title' => '',
     *                      'save_as'=> '',
     *                      'input_type' => '',
     *                      'params' => '',
     *                      'value' => '',
     *              ],
     *              ...
     *          ],
     *          'fields_rendered' => [
     *              // ключ = ID поля
     *              1 => 'html блок с полем'
     *          ]
     *
     *     ]
     *      ....
     * ]
     */
    public $field_list_full = [];

    /**
     * Модель поля
     */
    public $model_field;

    /**
     * Сведения о полях
     */
    public $field_info_list = [];

    /**
     * Отрендеренное поле
     */
    public $field_rendered = '';

    /**
     * Хеш формы
     */
    public $form_hash = '';

    /**
     * Рерультат получения сведений о компоненте по ID
     */
    public $xanyfield_get_component_info_result = [];

    /**
     * Результат сохранения значения поля
     */
    public $save_afield_field_value_result = [];

    /**
     * Модель валидатора поля
     */
    public $field_validator_model;

    /**
     * Массив отрендеренных полей
     */
    public $field_rendered_list = [];

    /**
     * Список провалидированных значений
     */
    public $validated_values = [];  

    /**
     * Возвращает отрендеренную форму c дополнительными полями
     */
    /*
    public function renderForm()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!is_string($this->component))
        {
            $this->addError('component', 'Компонент должен быть строкой');

            return false;
        }

        if (!is_string($this->component_category))
        {
            $this->addError('component_category', 'Категория компонента должна быть строкой');

            return false;
        }

        $this->value_assign_key = intval($this->value_assign_key);

        if (!strlen($this->component))
        {
            $this->addError('component', 'Не указан компонент');

            return false;
        }

        if (!strlen($this->component_category))
        {
            $this->addError('component_category', 'Не указана категория компонента');

            return false;
        }

        if (!$this->value_assign_key)
        {
            $this->addError('value_assign_key', 'Ключ привязки значения поля не указан');

            return false;
        }

        // Проверка существования компонента xanyfield
        $this->xanyfield_is_component_exists_result = \xtetis\xanyfield\models\SqlModel::xanyfieldIsComponentExists(
            $this->component,
            $this->component_category
        );

        // Если компонент xanyfield не существует
        if ($this->xanyfield_is_component_exists_result['result'] < 0)
        {
            $this->addError('component', $this->xanyfield_is_component_exists_result['result_str']);

            return false;
        }

        // ID компонента (ключ записи из xanyfield_component)
        $this->id_component = $this->xanyfield_is_component_exists_result['result'];

        // Получаем список полей для указанного компонента
        $this->xanyfield_get_field_list_by_component_result = \xtetis\xanyfield\models\SqlModel::xanyfieldGetFieldListByComponent(
            $this->id_component
        );

        // Генерирует массив со списком отрендеренных полей
        if (!$this->setFieldRenderedList())
        {
            return false;
        }

        // Генерирует хеш формы
        if (!$this->setFormHash())
        {
            return false;
        }

        // Урл страницы для валидации формы
        $url_validate_form = \xtetis\xanyfield\Component::makeUrl([
            'path' => [
                'form',
                'ajax_validate_form',
            ],
        ]);

        return \xtetis\xanyfield\Component::renderBlock(
            'form/form',
            [
                'field_list'        => $this->field_rendered_list,
                'id_component'      => $this->id_component,
                'value_assign_key'  => $this->value_assign_key,
                'form_hash'         => $this->form_hash,
                'url_validate_form' => $url_validate_form,
            ]
        );
    }
    */

    /**
     * Генерирует хеш формы
     */
    /*
    public function setFormHash()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->form_hash = md5($this->component . $this->component_category . $this->value_assign_key);

        return $this->form_hash;
    }
    */

    /**
     * Рендерим список полей
     */
    /*
    public function setFieldRenderedList()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->field_rendered_list = [];
        foreach ($this->xanyfield_get_field_list_by_component_result as &$item)
        {
            $item['id_field'] = intval($item['id_field']);
            if (!$this->getRenderedField($item['id_field']))
            {
                return false;
            }

        }

        return true;
    }
    */


    /**
     * Валидация формы
     */
    public function validateForm()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $id_component     = \xtetis\xengine\helpers\RequestHelper::post('id_component', 'str', '');
        $value_assign_key = \xtetis\xengine\helpers\RequestHelper::post('value_assign_key', 'str', '');
        $form_hash        = \xtetis\xengine\helpers\RequestHelper::post('form_hash', 'str', '');

        $this->id_component     = intval($id_component);
        $this->value_assign_key = intval($value_assign_key);

        if (!$this->value_assign_key)
        {
            $this->addError('common', __FUNCTION__ . ': Не указан value_assign_key');

            return false;
        }

        if (!$this->id_component)
        {
            $this->addError('common', __FUNCTION__ . ': Не указан id_component');

            return false;
        }

        // Проверка существования компонента xanyfield и получение параметров компонента
        $this->xanyfield_get_component_info_result = \xtetis\xanyfield\models\SqlModel::xanyfieldGetComponentInfo(
            $this->id_component
        );

        // Если компонент xanyfield не существует
        if ($this->xanyfield_get_component_info_result['result'] < 0)
        {
            $this->addError('common', $this->xanyfield_get_component_info_result['result_str']);

            return false;
        }

        $this->component          = $this->xanyfield_get_component_info_result['result_str'];
        $this->component_category = $this->xanyfield_get_component_info_result['category'];

        // Генерирует хеш формы
        if (!$this->setFormHash())
        {
            return false;
        }

        if ($this->form_hash != $form_hash)
        {
            $this->addError('common', 'Некорректный хэш формы');

            return false;
        }

        // Валидируем значения полей
        if (!$this->validateFields())
        {
            return false;
        }

        // Сохраняем значения полей
        if (!$this->saveFields())
        {
            return false;
        }

        return true;

    }

    /**
     * Валидирует все поля
     */
    public function validateFields()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Получаем список полей для указанного компонента
        $this->xanyfield_get_field_list_by_component_result = \xtetis\xanyfield\models\SqlModel::xanyfieldGetFieldListByComponent(
            $this->id_component
        );

        foreach ($this->xanyfield_get_field_list_by_component_result as &$item)
        {
            $item['id_field'] = intval($item['id_field']);

            $field_value = \xtetis\xengine\helpers\RequestHelper::post('xanyfield_' . $item['id_field'], 'raw', '');

            $params = [
                'id'               => $item['id_field'],
                'id_component'     => $this->id_component,
                'value_assign_key' => $this->value_assign_key,
                'value'            => $field_value,
            ];
            $this->model_field = new \xtetis\xanyfield\models\FieldModel($params);

            if (!$this->model_field->validateValue())
            {
                $this->addError('xanyfield_' . $item['id_field'], $this->model_field->getLastErrorMessage());

            }

            $this->validated_values[$item['id_field']] = $this->model_field->value;
        }

        return true;
    }

    /**
     * Сохраняет значение полей
     */
    public function saveFields()
    {

        if ($this->getErrors())
        {
            return false;
        }

        foreach ($this->xanyfield_get_field_list_by_component_result as &$item)
        {

            // Сохраняем значение поля
            $this->save_afield_field_value_result = \xtetis\xanyfield\models\SqlModel::xanyfieldSaveValue(
                $this->id_component,
                $item['id_field'],
                $this->value_assign_key,
                $this->validated_values[$item['id_field']]
            );

            // Если компонент xanyfield не существует
            if ($this->save_afield_field_value_result['result'] < 0)
            {
                $this->addError('xanyfield_' . $item['id_field'], $this->save_afield_field_value_result['result_str']);

                return false;
            }
        }

        return true;
    }

    /**
     * Рендерит поле HTML
     *
     * Должны быть предварительно заполнены
     *      $this->id_component
     *      $this->value_assign_key
     */
    public function getRenderedField($id_field)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_component     = intval($this->id_component);
        $this->value_assign_key = intval($this->value_assign_key);

        if (!$this->id_component)
        {
            $this->addError('id_component', __FUNCTION__ . ': не указан id_component');

            return false;
        }

        if (!$this->value_assign_key)
        {
            $this->addError('id_component', __FUNCTION__ . ': не указан value_assign_key');

            return false;
        }

        $id_field = intval($id_field);

        $params = [
            'id'               => $id_field,
            'id_component'     => $this->id_component,
            'value_assign_key' => $this->value_assign_key,
        ];
        $this->model_field    = new \xtetis\xanyfield\models\FieldModel($params);
        $this->field_rendered = $this->model_field->renderField();
        if (!$this->field_rendered)
        {
            $this->addError('model_field', __FUNCTION__ . ': Ошибка при создании поля #' . $id_field . ': ' . $this->model_field->getLastErrorMessage());

            return false;
        }

        $this->field_rendered_list[$id_field] = $this->field_rendered;

        return $this->field_rendered;
    }
}
