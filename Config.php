<?php

namespace xtetis\xanyfield;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        if (!defined('XANYFIELD_PARENT_GALLERY_CATEGORY'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа XANYFIELD_PARENT_GALLERY_CATEGORY');
        }

        if (!is_integer(XANYFIELD_PARENT_GALLERY_CATEGORY))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа XANYFIELD_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }

        if (!XANYFIELD_PARENT_GALLERY_CATEGORY)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа XANYFIELD_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }

        $model_gallery = \xtetis\ximg\models\CategoryModel::generateModelById(XANYFIELD_PARENT_GALLERY_CATEGORY);
        if (!$model_gallery)
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Константа XANYFIELD_PARENT_GALLERY_CATEGORY должна быть сущесвующей категорией');
        }
    }

}
